﻿using System.Collections.Generic;
using OfficeOpenXml;

namespace OmniarchiveIndexParser
{
    class SheetTools
    {
        public static string GetHyperlinkedText(ExcelRange cells, string cellAddress)
        {
            if (cells[cellAddress].Formula != "")
                return $"[{cells[cellAddress].Text}]({cells[cellAddress].Formula.Replace("HYPERLINK(\"", "").Replace($"\",\"{cells[cellAddress].Text}\")", "")})";
            return cells[cellAddress].Text;
        }
        public static string GetCellColor(ExcelRange cells, string column, int row)
        {
            if (cells[$"{column}{row}"].Style.Fill.BackgroundColor.Rgb != null)
                return cells[$"{column}{row}"].Style.Fill.BackgroundColor.Rgb[2..];
            return "ff00ff";
        }
        // public static string GetVersionColor(ExcelRange cells, int row)
        // {
        //     if (cells[$"D{row}"].Style.Fill.BackgroundColor.Rgb != null)
        //         return cells[$"D{row}"].Style.Fill.BackgroundColor.Rgb[2..];
        //     return "ff00ff";
        // }
        // private static ExcelRange GetCellSafe(ExcelRange range, string col, int row)
        // {
        //     if (row <= 2) return range["a1"];
        //     int srow = row;
        //     ExcelRange ce = range[col + row];
        //     while (ce.Merge && ce.Text == null) ce = range[col + --srow];
        //     return ce;
        // }
        // public static string GetVal(ExcelRange @this)
        // {
        //     if (@this.Merge)
        //     {
        //         var idx = @this.Worksheet.GetMergeCellId(@this.Start.Row, @this.Start.Column);
        //         string mergedCellAddress = @this.Worksheet.MergedCells[idx - 1];
        //         string firstCellAddress = @this.Worksheet.Cells[mergedCellAddress].Start.Address;
        //         return @this.Worksheet.Cells[firstCellAddress].Value?.ToString()?.Trim() ?? "";
        //     }
        //
        //     return @this.Value?.ToString()?.Trim() ?? "";
        // }
    }

    public static class SheetUtils
    {
        public static List<string> types = new() {"Pre-Classic", "Classic", "Indev", "Infdev", "Alpha", "Beta","b1.6", "b1.8",
            "Release", "1.0", "1.1","1.2", "1.3", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9", "1.10", "1.11", "1.12",
            "1.13", "1.14"};
        public static int IndexOfType(this string text)
        {
            if (types.Contains(text)) return types.IndexOf(text);
            if (text.StartsWith('b')) return types.IndexOf("Beta");
            if (text.StartsWith("1.")) return types.IndexOf("Release");
            for (int i = 0; i < types.Count; i++)
            {
                if (text.StartsWith(types[i])) return i;
            }
            return -1;
        }
    }
}
