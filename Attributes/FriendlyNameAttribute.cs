using System;

[AttributeUsage(AttributeTargets.Property)]
public class FriendlyNameAttribute : Attribute
{
    public string Name;

    public FriendlyNameAttribute(string name)
    {
        this.Name = name;
    }
}