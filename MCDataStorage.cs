﻿using DSharpPlus.Entities;
using Newtonsoft.Json;

namespace OmniarchiveIndexParser
{
    public class McInfo
    {
        public string GamePlatform { get; set; }
        public string Environment { get; set; }
        [JsonIgnore]
        public string EntryType { get; set; }
        [FriendlyName("Release cycle")]
        public string Type { get; set; }
        [FriendlyName("ID")]
        public string Id { get; set; }
        [FriendlyName("Version")]
        public string Version { get; set; }
        [FriendlyName("PVN")]
        public string Pvn { get; set; }
        [FriendlyName("Released")]
        public string ReleaseDate { get; set; }
        [FriendlyName("Proof")]
        public string ProofOfExistence { get; set; }
        [FriendlyName("Notes")]
        public string Notes { get; set; }
        [FriendlyName("MD5")]
        public string Md5 { get; set; }
        [FriendlyName("SHA-256")]
        public string Sha256 { get; set; }
        [FriendlyName("Color")]
        public string Color { get; set; }
        [FriendlyName("Platform")]
        public string Format { get; set; }
        public string Os { get; set; }
        public string FoundStatus
        {
            get
            {
                return
                    Color == "93C47D" ? "Found" :
                    Color == "6FA8DC" ? "Edited" :
                    Color == "6D9EEB" ? "Edited" : //java servers uses a different color for some reason
                    Color == "E06666" ? "Unfound" :
                    Color == "FFD966" ? "No proof" :
                                        "Unknown?";
            }
        }

        public DiscordEmbed GetVersionEmbed()
        {
            //Console.WriteLine($"Found possible version:\n{JsonConvert.SerializeObject(this, Formatting.Indented)}");
            DiscordEmbedBuilder embed = new DiscordEmbedBuilder();
            if (!string.IsNullOrEmpty(EntryType) )
                embed.AddField("Entry type", EntryType.Replace("Clients", "Client").Replace("Servers", "Server"), true);
            if (!string.IsNullOrEmpty(Type))
                embed.AddField("Type", Type, true);
            if (!string.IsNullOrEmpty(Id))
                embed.AddField("ID", Id, true);
            if (!string.IsNullOrEmpty(Version))
                embed.AddField("Version", Version, true);
            if (!string.IsNullOrEmpty(Pvn))
                embed.AddField("PVN", $"`{Pvn}`", true);
            if (!string.IsNullOrEmpty(ReleaseDate))
                embed.AddField("Release Date", ReleaseDate, true);
            if (!string.IsNullOrEmpty(Format))
                embed.AddField("Format", Format, true);
            if (!string.IsNullOrEmpty(Os))
                embed.AddField("OS", Os, true);
            if (!string.IsNullOrEmpty(ProofOfExistence))
                embed.AddField("Proof of existence", ProofOfExistence, true);
            if (!string.IsNullOrEmpty(Notes))
                embed.AddField("Notes", Notes, true);
            if (!string.IsNullOrEmpty(Md5))
                embed.AddField("MD5 hash", $"`{Md5}`");
            if (!string.IsNullOrEmpty(Sha256))
                embed.AddField("SHA-256 hash", $"`{Sha256}`", true);
            embed.WithColor(new DiscordColor(Color));
            return embed.Build();
        }

    }
}
